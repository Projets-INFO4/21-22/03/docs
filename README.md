# Project Robin

Florent BARBE | Georges-Harrisson SIMO YOKAM | Thomas BACH  

## Logbook :

* 17/01/2022 : Presentation about LoRaWAN  
TODO : Contact 5th year students that have worked on this project to discuss about what it can be rework/upgrade, and get the equipment at the fablab.

* 24/01/2022 : We picked up the STM32 microcontroller, and start researches about the how to use the board and the IDE. We also discussed with the 5th year students for a further understanding of the subject.  
TODO : Start to get used to the work environment.

* 31/01/2022 : Discussions with M.Donsez about the subject (precisions of the subject because the work done last year was a bit confusing). Beginning of researche about AI with STM32 boards, and neural network models.  
TODO : Get the LED working with a simple program (blinking)

* 07/02/2022 : Tests and configuration in STM32CUBEIDE, the LED blinks now.  
TODO : Configure the microphones on the microcontroller, and try to follow the tutorial about audio commands recognition with a simple neural network. Start thinking about making our own network.

* 14/02/2022 : We got a new microcontroller : the WIO terminal (see doc in links part). We ran a lot of examples on the website (hello world, reading microphone, displaying frequencies...)  
TODO : Follow tutorials on website and apply them to sound recognition

* 28/02/2022 : Midterm presentation at the beginning. Split the work in 2 groups : Georges-Harrisson started to make a model with EdgeImpulse and Florent/Thomas thought about menu configuration and implementation (file menu_diagram.pdf). We talked about dataset format and how to gather audio samples (internet or record by ourselves). Tutos followed :
    - Audio scene recognition : https://wiki.seeedstudio.com/Wio-Terminal-TinyML-EI-3/  
    - UI on Wio Terminal : https://youtu.be/P8CQtByWfS4    

* 07/03/2022 : We tried to get familar with LVGL library for Wio Terminal, which allows us to make GUI optimized for microcontrollers. We tested the library with some very basic examples such as buttons, labels...

* 08/04/2022 : The main menu is working (with 6 sub menus to implement). We tried to add the audio spectrum code as a sub menu to the application, it is almost working (background problems). Also the code structure needs to be reorganized.  

* 14/03/2022 : Reorganization of the code (main .ino file, main gui.cpp file that ticks the other .cpp files when the menu is used), partial correction of the background problems with the spectrum screen.

* 15/03/2022 : Creation and training of a simple neural network to recognize robin, dog and gun sounds using EdgeImpulse.  

* 19/03/2022 : Add more samples to robin, gunshots and barking labels. Tests on different EdgeImpulse parameters (number of training cycles, low frequency...)to increase accuracy of the model. Move sounds folder in the docs repo.
Notice that some gunshot sounds are actually reloading sounds, that is probably because of theses reloadings that our model is sometimes confused between robin and gunshots.

* 21/03/2022 : The wioterminal detects robin, gun and barking sounds using a new .ino file and the new neural network, exported as an Arduino library.  

* 22/03/2022 : 2 new branches created : one for loading screen and fixing menu flicking when switching to spectrum submenu, and one for integrating continuous detection to the menu.  
Ideas to fix menu : try to reduce lvgl task priority for menu (maybe the flick is due to menu tick between spectrum setup and tick)  
We discussed with M.Donsez about how to found datasets (wikimedia), and also testing STM32 microphones which are better than the one on the Wio Terminal.  

* 26/03/2022 : The menu switching problem is fixed (no longer weird black transition when selecting spectrum) and an initializing screen has been added (appears when the program is launched). The init_screen branch has been merged to main branch.
Things to do on monday and tuesday :
    - Train the model with additionnal sounds
    - Work on the continuous detection submenu integration into the main application
    - Work on the LoRaWAN feature  

* 28/03/2022 : Work on continuous detection integration into the main app. Created a class "Continuous" for the submenu, the code has been adapted to work along with the menu and everything should be fine but we ended up having libraries problems. We spent several hours trying to solve these problems but we couldn't figure out how to fix them.
We can still launch the menu app and the continuous detection app independently though.  
In the meantime, we started thinking about the general structure of our report (table of contents, images, diagrams...)  

* 29/03/2022 : We tried again to fix that library problem, without sucess. So we decided to add more samples and labels to our NN model. New labels : crow, boar, wolf, stag, great tit and room. We also tweaked some parameters to improve our model accuracy.  
In addition, progress has been made on the report writting.  

## Links / Bibliography :
- STM32 board : https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html
- Tuto LED blink for STM32 board : https://wiki.st.com/stm32mcu/wiki/STM32StepByStep:Step2_Blink_LED
- Video about deX-CUBE-AI : https://youtu.be/crJcDqIUbP4
- Examples of AI projects with tensorflow https://www.tensorflow.org/lite/examples?hl=fr  
- Tuto simple audio commands recognition : https://www.tensorflow.org/tutorials/audio/simple_audio?hl=fr
- Documentation and tutos for WIO board : https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/
- Transfer Learning for the Audio Domain with TensorFlow Lite Model Maker : https://www.tensorflow.org/lite/tutorials/model_maker_audio_classification
- Transfer learning with YAMNet for environmental sound classification : https://www.tensorflow.org/tutorials/audio/transfer_learning_audio?hl=en
- Articles about CNN and audio classification : https://towardsdatascience.com/audio-deep-learning-made-simple-sound-classification-step-by-step-cebc936bbe5  
https://towardsdatascience.com/cnns-for-audio-classification-6244954665ab  


## LoRaWAN notes :

Limited : not suitable to video or sound sending unlike Wi-Fi.
It can takes up to 2s to send only a few bytes.

That's the reason our project aims to process and compute data within the board and send only results (type of sounds identified, number of animals, gunfires by hunters...)

LoRaWAN : low rate of flow, wide scope if no obstruction, and low energy consumption.
Frequency : 868 MHz.
LPWAN networks: Low-Power Wide Area Network.
