import librosa
import sys
import soundfile as sf

input_filename = sys.argv[1]
output_filename = sys.argv[2]

data, samplerate = librosa.load(input_filename, sr=16000)
print(data.shape, samplerate)

sf.write(output_filename, data, samplerate, subtype='PCM_16')